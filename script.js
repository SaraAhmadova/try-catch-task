// Display this array as a list on the screen (using the <ul> tag - the list should be generated using JavaScript).
// There should be a div on the page with id="root", where this list should be added (similar to the task performed in the basic module).
// Before adding an object to the page, it should be checked for correctness (the object should contain all three properties - author, name, price).
// If any of these properties are missing, an error should be displayed in the console indicating which property is missing in the object.
// Array elements that do not meet the conditions mentioned above should not appear on the page.
const books = [
  {
    author: "Lucy Foley",
    name: "List of Invitees",
    price: 70,
  },
  {
    author: "Susanna Clarke",
    name: "Jonathan Strange & Mr Norrell",
  },
  {
    name: "Design. A Book for Non-Designers.",
    price: 70,
  },
  {
    author: "Alan Moore",
    name: "Neonomicon",
    price: 70,
  },
  {
    author: "Terry Pratchett",
    name: "Moving Pictures",
    price: 40,
  },
  {
    author: "Angus Hyland",
    name: "Cats in Art",
  },
];

const createListItem = (itemData) => {
  const listItem = document.createElement("li");

  try {
    if(!itemData.name) throw new Error("Book name is missing!");
    if(!itemData.author) throw new Error("Book author is missing!");
    if(!itemData.price) throw new Error("Book price is missing!");

    const name = document.createElement("div");
    const author = document.createElement("div");
    const price = document.createElement("div");
    name.textContent = `name: ${itemData.name}`;
    author.textContent = `author: ${itemData.author}`;
    price.textContent = `price: ${itemData.price}`;
    listItem.appendChild(name);
    listItem.appendChild(author);
    listItem.appendChild(price);
    list.appendChild(listItem);
    
  } catch (error) {
    console.log(error);
  }
};

const list = document.createElement("ul");
document.querySelector("#root").appendChild(list);

books.forEach((book) => {
  createListItem(book);
});
